import urllib.request
import re
import chardet
import operator
import encoding

file_urls_json = [
  "https://raw.githubusercontent.com/netology-code/Python_course/master/PY1_Lesson_2.3/newsit.json",
  "https://raw.githubusercontent.com/netology-code/Python_course/master/PY1_Lesson_2.3/newsafr.json",
  "https://raw.githubusercontent.com/netology-code/Python_course/master/PY1_Lesson_2.3/newscy.json",
  "https://raw.githubusercontent.com/netology-code/Python_course/master/PY1_Lesson_2.3/newsfr.json"
  ]

encoding.run(file_urls_json)