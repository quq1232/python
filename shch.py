import chardet
import urllib.request
import re
import string
# import pprint

def get_filename_from_url(url):
  regexp = r'(?:[^/][\d\w\.]+)$'
  return re.search(regexp, url).group(0).split('.')[:-1][0] + '.txt'

url_prefix = 'http://blackalpinist.com/scherbakov/'
alphabetic = url_prefix + 'alphabetic.html'
debug_filename = 'index.html'

with urllib.request.urlopen(alphabetic) as response, open(debug_filename, 'wb') as out_file:
  data = response.read()
  encoding = chardet.detect(data)
  # out_file.write(data.decode(encoding['encoding']).encode())
  html = data.decode(encoding['encoding'])
  html_endoded = html.encode()
  out_file.write(html_endoded)

'''
<p><a href="./htmtexts/1994/theraven.html">The Raven</a>&nbsp;&nbsp;<a href="./htmtexts/1994/index.html">(1994)</a><br>
'''
regexp1 = r'(?<=="\.\/)htmtexts\/.*?\/(?!index).*?html'
links = re.findall(regexp1, html)

regexp2 = r'(?<=<pre>).*(?=<\/pre>)'
regexp3 = r'&.*?;'
regexp4 = ';| |,|\n|\r|-|\.|\!|\?|\t'

words = []

finish = set()

i = 0
for link in links:
  i += 1
  url = url_prefix + link
  # print(url)
  # print(get_filename_from_url(url))
  with urllib.request.urlopen(url) as resp, open(get_filename_from_url(url), 'wb') as out_file:
    data = resp.read()
    encoding = chardet.detect(data)
    # out_file.write(data.decode(encoding['encoding']).encode())
    file = data.decode(encoding['encoding'])
    text = re.sub(regexp3, '', re.search(regexp2, file, flags=re.S).group(0))
    out_file.write(text.encode())
    # words.extend(text.split(' '))
    words.extend(re.split(regexp4, text))
    # print(words)
    for word in words:
      if word:
        # print('first letter of word {} is {}'.format(word, word[0]))
        if word[0].lower() == 'ф':
          finish.add(word)
  if i > 500:
    break

# print(finish)
list(finish).sort()
print(finish)