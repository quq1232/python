import os
from os.path import dirname, exists, isfile, join
import subprocess

def init_dirs():
  source_folder = "Source"
  result_folder = "Result"
  sources = join(dirname(__file__), source_folder)
  results = join(dirname(__file__), result_folder)
  return sources, results

def get_pic_list(dir_from):
  try:
    pic_list = [f for f in os.listdir(dir_from) if isfile(join(dir_from, f))]
  except:
    print("no source files")
    pic_list = []
  return pic_list

def convert(pic_list, dir_from, dir_to):
  if not exists(dir_to):
    os.makedirs(dir_to)
  for pic in pic_list:
    subprocess.run(["convert", join(dir_from, pic), "-resize", "200", join(dir_to, pic)])

def run():
  sources, results = init_dirs()
  pictures = get_pic_list(sources)
  if pictures:
    convert(pictures, sources, results)

run()