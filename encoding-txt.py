import urllib.request
import re
import chardet
import operator
import encoding

'''
debug_level = 0

def dprint(log):
  if debug_level != 0:
    print(log)
'''
txt_file_urls = [
  "https://raw.githubusercontent.com/netology-code/Python_course/master/PY1_Lesson_2.3/newsafr.txt",
  "https://raw.githubusercontent.com/netology-code/Python_course/master/PY1_Lesson_2.3/newsit.txt",
  "https://raw.githubusercontent.com/netology-code/Python_course/master/PY1_Lesson_2.3/newscy.txt",
  "https://raw.githubusercontent.com/netology-code/Python_course/master/PY1_Lesson_2.3/newsfr.txt"
  ]
'''
def get_filename_from_url(url):
  regexp = r'(?:[^/][\d\w\.]+)$(?<=(?:.txt))'
  return re.search(regexp, url).group(0) 

def copy_files(urls):
  files = []
  for url in urls:
    filename = encoding.get_filename_from_url(url)
    dprint("{} downloaded".format(filename))
    with urllib.request.urlopen(url) as response, open(filename, 'wb') as out_file:
      data = response.read()
      out_file.write(data)
      files.append(filename)
  dprint(files)
  return files

def collect_words_from_file(file):
  words = []
  with open(file, 'rb') as f:
    data = f.read()
    result = chardet.detect(data)
    # dprint(data.decode(result['encoding']))
    words.extend(data.decode(result['encoding']).split(' '))
  return words

def get_words_dict(words):
  words_dict = dict() 
  for word in words:
    if len(word) < 6:
      dprint("word [{}] is out of rule".format(word))
      continue
    if word in words_dict:
      words_dict[word] = words_dict[word] + 1
    else:
      words_dict[word] = 1
  return words_dict

def sort_dict_of_word(d):
  # for key, value in d.items():
    # if value < 10:
      # dprint("remove word {} as it has only {} appearances".format(key, value))
      # d.pop(key)
  sorted_dictionary = sorted(d.items(), key=lambda x: x[1], reverse=True)
  return sorted_dictionary

def print_first_ten(d):
  for i in range(0,10):
    if i == len(d):
      break
    dprint(d[i])
    print("word {} appears {} times".format(d[i][0], d[i][1]))

def run():
  files = encoding.copy_files(txt_file_urls)
  for file in files:
    words = encoding.collect_words_from_file(file)
    dprint(len(words))
    word_dict = encoding.get_words_dict(words)
    dprint(word_dict)
    sorted_dict = encoding.sort_dict_of_word(word_dict)
    dprint(sorted_dict)
    print("\nresults for file {}:".format(file))
    encoding.print_first_ten(sorted_dict)
'''
encoding.run(txt_file_urls)