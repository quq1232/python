import chardet
import urllib.request
import re
import string
# import pprint

def get_url_and_file():
  from_url = input("Enter url from: \n")
  to_file = input("Enter filename to: \n")
  return from_url, to_file
  

def get_file_from_web(from_url, to_file):
  try:
    with urllib.request.urlopen(from_url) as response, open(to_file, 'wb') as out_file:
      data = response.read()
      encoding = chardet.detect(data)
      # out_file.write(data.decode(encoding['encoding']).encode())
      html = data.decode(encoding['encoding'])
      html_endoded = html.encode()
      out_file.write(html_endoded)
  except Exception as e:
    # raise e
    print(e)
    exit(1)


def run():
  from_url, to_file = get_url_and_file()
  get_file_from_web(from_url, to_file)

# get_file_from_web('http://10.100.109.29:7009/apigate/ws/soap?wsdl', 'schema.wsdl')
run()