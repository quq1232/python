import chardet
import os

def remove_file(myfile):
  if os.path.isfile(myfile):
    os.remove(myfile)
  else:
    print("Error: {%s} file not found".format(myfile))

def convert_file(file_in, file_out):
  with open(file_in, 'rb') as fin, open(file_out, 'ab') as fout:
    data = fin.read()
    print(data)
    result = chardet.detect(data)
    print(result)
    if result['encoding'] is None:
      print(":(")
      fout.close()
      remove_file(file_out)
      return
    data = data.decode(result['encoding'])
    fout.write(data)

convert_file('VirtCard.log', 'VirtCard2.log')
