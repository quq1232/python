# Задание
# мне нужно отыскать файл среди десятков других
# я знаю некоторые части этого файла (на память или из другого источника)
# я ищу только среди .sql файлов
# 1. программа ожидает строку, которую будет искать (input())
# после того, как строка введена, программа ищет её во всех файлах
# выводит список найденных файлов построчно
# выводит количество найденных файлов
# 2. снова ожидает ввод
# поиск происходит только среди найденных на этапе 1
# 3. снова ожидает ввод
# ...
# Выход из программы программировать не нужно.
# Достаточно принудительно остановить, для этого можете нажать Ctrl + C

# Пример на настоящих данных

# python3 find_procedure.py
# Введите строку: INSERT
# ... большой список файлов ...
# Всего: 301
# Введите строку: APPLICATION_SETUP
# ... большой список файлов ...
# Всего: 26
# Введите строку: A400M
# ... большой список файлов ...
# Всего: 17
# Введите строку: 0.0
# Migrations/000_PSE_Application_setup.sql
# Migrations/100_1-32_PSE_Application_setup.sql
# Всего: 2
# Введите строку: 2.0
# Migrations/000_PSE_Application_setup.sql
# Всего: 1

# не забываем организовывать собственный код в функции

import os
from os import listdir
from os.path import join, isfile, splitext
import re
import chardet

my_version = '2.00'
migrations = 'Migrations'
current_dir = os.path.dirname(os.path.abspath(__file__))
# print(f'current_dir={current_dir}')

def get_file_extension(filename, mode='regexp'):
  # print(f'mode={mode}')
  if mode == 'regexp':
    regexp = r'(\.\w+)$'
    try:
      extension = re.search(regexp, filename).group(0)
    except AttributeError:
      extension = ''
  elif mode == 'splitext':
    try:
      extension = splitext(filename)[1]
    except ValueError:
      extension = ''
  else:
    print(f'Undescribed mode to get extension: [{mode}]')
    exit(1)
  # print(f'extension={extension}')
  return extension


def print_files_with_total(files):
  for file in files:
    print(join(migrations, file))
  print(f'Bceго: {len(files)}')

def is_suitable_file(file):
  return isfile(file) and get_file_extension(file, mode='splitext') == '.sql'

if __name__ == '__main__':
  sql_files = [f for f in listdir(join(current_dir, migrations)) 
    if is_suitable_file(join(current_dir, migrations, f))]

  while True:
    sql_files_new = []
    searched = input('Введите строку: ').lower()
    for sql_file in sql_files:
      with open(join(current_dir, migrations, sql_file), 'rb') as f:
        data = f.read()
        result = chardet.detect(data)
        data = data.decode(result['encoding']).lower()
        if searched in data:
          sql_files_new.append(sql_file)
    sql_files = sql_files_new
    print_files_with_total(sql_files)
