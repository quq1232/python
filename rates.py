import urllib.request
from pprint import pprint
import re
import chardet
import operator
import encoding
import osa
import requests
import xml.etree.ElementTree as ET
from functools import reduce
import me

print = me.logger(print, me.DEBUG_LEVELS["PROD"])


file_urls = {
  'currencies': 'https://raw.githubusercontent.com/netology-code/Python_course/master/homework/3.4-currencies/currencies.txt',
  'temps': 'https://raw.githubusercontent.com/netology-code/Python_course/master/homework/3.4-currencies/temps.txt',
  'travel': 'https://raw.githubusercontent.com/netology-code/Python_course/master/homework/3.4-currencies/travel.txt',
  }

soap_urls = {
  'temps' : 'https://www.w3schools.com/xml/tempconvert.asmx',
  'currencies' : 'https://fx.currencysystem.com/webservices/CurrencyServer4.asmx',
}

def get_filename_from_url(url):
  regexp = r'(?:[^\/][\d\w\.]+)$'
  filename = re.search(regexp, url).group(0) 
  return filename

def copy_file(url):
  filename = get_filename_from_url(url)
  with urllib.request.urlopen(url) as response, open(filename, 'wb') as out_file:
    data = response.read()
    out_file.write(data)
  return filename

def get_first_figure_from_line(string):
  return string.split(' ')[0]

def convert_amounts(filename):
  rub_list = list()
  with open(filename, 'r') as amounts: 
    for line in amounts:
      rub_list.append(ConvertToNum(*get_amount_from_line(line)))
  return rub_list

def ConvertToNum(a,b):
  headers = {
    'Content-Type': 'text/xml; charset=utf-8',
  }
  data = f'''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cur="http://webservices.cloanto.com/currencyserver/">
   <soapenv:Header/>
   <soapenv:Body>
      <cur:ConvertToNum>
        <!--Optional:-->
         <cur:fromCurrency>{b}</cur:fromCurrency>
         <!--Optional:-->
         <cur:toCurrency>RUB</cur:toCurrency>
         <cur:amount>{a}</cur:amount>
         <cur:rounding>1</cur:rounding>
     </cur:ConvertToNum>
   </soapenv:Body>
</soapenv:Envelope>'''
  response = requests.post(soap_urls['currencies'], data=data, headers=headers)
  root = ET.fromstring(response.text)
  return float(root[0][0][0].text)

def get_amount_from_line(line):
  regexp = r'(\d+) (\w+)$'
  amount = re.search(regexp, line).group(1) 
  currency = re.search(regexp, line).group(2) 
  print(f'amount is [{amount} {currency}]') 
  return amount, currency

def FahrenheitToCelsius(a):
  headers = {
    'Content-Type': 'text/xml; charset=utf-8',
  }
  data = f'''<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <FahrenheitToCelsius xmlns="https://www.w3schools.com/xml/">
      <Fahrenheit>{a}</Fahrenheit>
    </FahrenheitToCelsius>
  </soap:Body>
</soap:Envelope>'''
  response = requests.post(soap_urls['temps'], data=data, headers=headers)
  root = ET.fromstring(response.text)
  return float(root[0][0][0].text)

def convert_fahrenheit_to_celsius(filename, mode='clever'):
  if mode not in ('clever', 'each'):
    print(f'Undescribed mode to calculate: [{mode}]', debug_level=me.DEBUG_LEVELS["ALL"])
    exit(1)
  temp_list = list()
  with open(filename, 'r') as temps: 
    for line in temps:
      # print(client_temps.service.FahrenheitToCelsius(line.split(' ')[0]), debug_level=me.DEBUG_LEVELS["TEST"])
      if mode == 'each':
        temp_list.append(FahrenheitToCelsius(get_first_figure_from_line(line)))
      elif mode == 'clever':
        temp_list.append(int(get_first_figure_from_line(line)))
  return temp_list

'''
client_temps = osa.Client(soap_urls['temps'] + '?wsdl')
# help(client_temps.service)

filename = 'temps.txt'
with open(filename, 'r') as temps: 
  for line in temps:
    print(f"[{line.split(' ')[0]}]", debug_level=me.DEBUG_LEVELS["TEST"])
    # print(client_temps.service.CelsiusToFahrenheit('68'), debug_level=me.DEBUG_LEVELS["TEST"])
    # print(client_temps.service.FahrenheitToCelsius('68'), debug_level=me.DEBUG_LEVELS["TEST"])
'''

def run():
  currencies = copy_file(file_urls['currencies'])
  temps = copy_file(file_urls['temps'])
  travel = copy_file(file_urls['travel'])

  # mode = 'each'
  mode = 'clever'

  if mode == 'clever':
    t2_list = convert_fahrenheit_to_celsius('temps.txt', mode=mode)
    avg_temp2 = FahrenheitToCelsius(reduce(lambda x,y: x + y, t2_list) / len(t2_list))
    print(f'Average temperature is {avg_temp2} Celcius', debug_level=me.DEBUG_LEVELS["PROD"])
  else:
    t_list = convert_fahrenheit_to_celsius('temps.txt', mode=mode)
    avg_temp = reduce(lambda x,y: x + y, t_list) / len(t_list)
    print(f'Average temperature is {avg_temp} Celcius', debug_level=me.DEBUG_LEVELS["PROD"])
  
  r_list = convert_amounts('currencies.txt')
  total = reduce(lambda x,y: x + y, r_list)
  print(f'Total cost is {total} RUB', debug_level=me.DEBUG_LEVELS["PROD"])

run()