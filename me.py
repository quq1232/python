DEBUG_LEVELS = {
  "NONE": 0, 
  "PROD": 1, 
  "TEST": 5, 
  "DEFUALT": 5, 
  "ALL": 9, 
}

def logger(f, threshold):
  saved_f = f
  def wrapper(*args, debug_level=None, **kwargs):
    if (debug_level is None) or (debug_level > DEBUG_LEVELS["ALL"]) or (debug_level < DEBUG_LEVELS["NONE"]):
      debug_level = DEBUG_LEVELS["DEFUALT"]
      # debug_level = threshold

    if threshold >= debug_level:
      result = f(*args, **kwargs)
    else:
      result = None
    return result
  return wrapper
