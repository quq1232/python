import requests
import chardet
# from pprint import pprint

def init(interface=None):
    marketing = '\n\nПереведено сервисом «Яндекс.Переводчик» => http://translate.yandex.ru/.'
    files = [
        'DE.txt', 
        'ES.txt', 
        'FR.txt',
        ]
    if not interface:
        interface = 'json'
    if interface == 'json':
        urls = {
            'getLangs': 'https://translate.yandex.net/api/v1.5/tr.json/getLangs', 
            'translate': 'https://translate.yandex.net/api/v1.5/tr.json/translate',
            'detect': 'https://translate.yandex.net/api/v1.5/tr.json/detect',
            }
        key = 'trnsl.1.1.20180615T100614Z.1d9d16d0912ae935.719f3e15d77bb192e77dbca9fd919437826d44b4'
    else:
        print("only json api supported")
        exit(1)
    return urls, key, marketing, files


def get_list_of_langs(url, key, ui=None):
    if not ui:
        ui = 'ru'
    params = {
        'key': key,
        'ui': ui,
        }
    response = requests.get(url, params=params).json()
    return response.get('langs')

def translate(url, key, text='hello world', lang_from=None, lang_to=None):
    """
    YANDEX translation plugin

    docs: https://tech.yandex.ru/translate/doc/dg/reference/translate-docpage/

    https://translate.yandex.net/api/v1.5/tr.json/translate ?
    key=<API-ключ>
     & text=<переводимый текст>
     & lang=<направление перевода>
     & [format=<формат текста>]
     & [options=<опции перевода>]
     & [callback=<имя callback-функции>]

    :param text: <str> text for translation.
    :return: <str> translated text.
    url = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
    key = 'trnsl.1.1.20161025T233221Z.47834a66fd7895d0.a95fd4bfde5c1794fa433453956bd261eae80152'
    """
    if not lang_to:
        lang_to='ru'

    if not lang_from:
        lang = lang_to
    else:
        lang = '-'.join((lang_from, lang_to))

    params = {
        'key': key,
        'lang': lang,
        'text': text,
        }
    response = requests.get(url, params=params).json()
    if not response.get('text'):
        return 'Unable to translate'
    return ' '.join(response.get('text', []))

# a = translate_it('hello world')
# print(a)

def get_text_from_file(file):
    with open(file, 'rb') as f:
        data = f.read()
        result = chardet.detect(data)
        data = data.decode(result['encoding'])
    # pprint(f"data from file {file} is:\n{data}")
    return data

def save_text_to_file(text, file):
    with open(file, 'w') as out_file:
      out_file.write(text)

def run():
    urls, key, footer, files = init()
    for file in files:
        text = get_text_from_file(file)
        ttext = translate(urls['translate'], key, text)
        save_text_to_file(ttext + footer, 't_'+file)

run()