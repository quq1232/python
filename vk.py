import requests
from pprint import pprint

APP_ID = "6609378"
API_VERSION = "5.80"
TOKEN = "961b68c81319b95fd16669dd946372edd3c3b48b2443d39acd7327dac71e623329e40caebe3f295790845"

# https://oauth.vk.com/authorize?client_id=6609378&display=page&scope=friends&response_type=token&v=5.80

vk_methods = {
  "getMutual": "https://api.vk.com/method/friends.getMutual", 
}

def get_mutual_friends(user1, user2):
  params = {
    "access_token": TOKEN,
    "source_uid": user1,
    "target_uid": user2,
    "v": API_VERSION,
  }
  response = requests.get(vk_methods["getMutual"], params).json()
  return response["response"]

friends = get_mutual_friends("2648383", "19472081")
pprint(dict(zip(friends, list(map(lambda x: "https://vk.com/id" + str(x), friends)))))