from PIL import Image

# http://pillow.readthedocs.org/en/latest/reference/Image.html#PIL.Image.format
# http://pillow.readthedocs.org/en/latest/handbook/image-file-formats.html

filename = input("enter filename\n")
img = Image.open(filename)
print("{} format is {}".format(filename, img.format))