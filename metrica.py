from pprint import pprint
import requests
import re

APP_ID = '751ccc42fe8d406fbc75b12c5d215e00'
# https://ququ1232.github.io/verify#access_token=AQAAAAAn4ZipAAUUKmoB34VvQ0YhmiZQaGfjZfY&token_type=bearer&expires_in=31536000
TOKEN = 'AQAAAAAn4ZipAAUUKmoB34VvQ0YhmiZQaGfjZfY'
methods = {
    'counters': 'https://api-metrika.yandex.ru/management/v1/counters',
    'counter': 'https://api-metrika.yandex.ru/management/v1/counter/',
    'data': 'https://api-metrika.yandex.ru/stat/v1/data'
}
metrics = 'ym:s:visits,ym:s:users,ym:s:pageviews'

class YandexMetricaManager:
    def __init__(self, token):
        self.token = token

    def get_headers(self):
        return {
            'Authorization': f'OAuth {TOKEN}'
        }

    def get_counters(self):
        response = requests.get(methods['counters'], headers=self.get_headers()).json()
        # pprint(response['counters'])
        return [c['id'] for c in response['counters']]

    def get_counter_info(self, counter_id):
        response = requests.get(''.join([methods['counter'], str(counter_id)]), headers=self.get_headers()).json()
        pprint(response)
        return 0

    def get_params(self, counter):
        return {
            'id': str(counter),
            'metrics': metrics
        }

    def get_metrics(self, counter_id):
        response = requests.get(methods['data'], headers=self.get_headers(), params=self.get_params(counter_id)).json()
        # pprint(response['totals'])
        pprint(list(zip(self.names(metrics.split(',')), response['totals'])))
        return 0

    def names(self, listic):
        output = list()
        regexp = r'(?:[^\:][\d\w]+)$'
        for metrica in listic:
            name = re.search(regexp, metrica).group(0)
            output.append(name)
        return output


user1 = YandexMetricaManager(TOKEN)
for counter in user1.get_counters():
    user1.get_metrics(counter)
